package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.general.AbstractEntity;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Genre implements AbstractEntity<Genre> {
    private Long id;
    private  String  genreName;

    @Override
    public Genre getCopy() {
        return new Genre(id, genreName);
    }
}
