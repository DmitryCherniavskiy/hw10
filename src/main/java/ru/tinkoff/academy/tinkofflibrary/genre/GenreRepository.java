package ru.tinkoff.academy.tinkofflibrary.genre;

import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.general.AbstractRepository;

import java.util.List;

@Repository
public class GenreRepository extends AbstractRepository<Genre> {

    public GenreRepository() {
        super(List.of(
                Genre.builder()
                        .id(1L)
                        .genreName("Horror")
                        .build(),
                Genre.builder()
                        .id(2L)
                        .genreName("Adventure")
                        .build()
        ));
    }
}
