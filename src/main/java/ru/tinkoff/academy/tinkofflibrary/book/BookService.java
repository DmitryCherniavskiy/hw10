package ru.tinkoff.academy.tinkofflibrary.book;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.author.Author;
import ru.tinkoff.academy.tinkofflibrary.author.AuthorService;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;
import ru.tinkoff.academy.tinkofflibrary.genre.GenreService;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookService {

    private final AuthorService authorService;
    private final GenreService genreService;

    private final BookRepository bookRepository;

    private final ObjectMapper objectMapper;

    public Book save(CreatingBookDto bookDto) {
        Author author = authorService.getById(bookDto.getAuthorId());
        Genre genre = genreService.getById(bookDto.getGenreId());

        Book bookForSaving = Book.builder()
                .id(1L)
                .name(bookDto.getName())
                .authorName(author.getAuthorName())
                .genreName(genre.getGenreName())
                .publicationDate(LocalDate.of(2020, 2, 2))
                .build();

        return bookRepository.save(bookForSaving);
    }

    public boolean removeById(Long bookId) {
        return bookRepository.removeById(bookId);
    }

    public Optional<Book> findById(Long bookId) {
        return bookRepository.findById(bookId);
    }

    public List<Book> findAll() {
        return bookRepository.getAll();
    }

    public List<Book> findByParam(Map<String,String> paramMap) {
        if (paramMap.isEmpty()){
            return bookRepository.getAll();
        }
        return bookRepository.getAll().stream()
                .map(book -> (Map<String,Object>) objectMapper.convertValue(book, Map.class))
                .filter(fields -> isCompareSearch(paramMap, fields))
                .map(fields -> objectMapper.convertValue(fields, Book.class))
                .toList();
    }

    private boolean isCompareSearch(Map<String, String> searchParam, Map<String, Object> fields){
        for (Map.Entry<String, Object> entry : fields.entrySet()){
            String searchValue = searchParam.get(entry.getKey());
            if (!entry.getValue().toString().equals(searchValue)){
                return false;
            }
        }
        return true;
    }

}
