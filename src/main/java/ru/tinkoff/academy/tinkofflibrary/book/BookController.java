package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping("/search")
    public List<Book> search(@RequestParam Map<String,String> requestParam) {
        return bookService.findByParam(requestParam);
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<Book> getById(@PathVariable Long bookId) {
        Optional<Book> book = bookService.findById(bookId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }

    @PostMapping
    public Book save(@RequestBody CreatingBookDto bookDto) {
        return bookService.save(bookDto);
    }

    @DeleteMapping("/bookId")
    public boolean deleteById(@PathVariable Long bookId){
        return bookService.removeById(bookId);
    }
}
