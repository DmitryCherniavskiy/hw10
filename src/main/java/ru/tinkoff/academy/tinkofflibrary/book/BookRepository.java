package ru.tinkoff.academy.tinkofflibrary.book;

import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.general.AbstractRepository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class BookRepository extends AbstractRepository<Book> {

    public BookRepository() {
        super(List.of(
                Book.builder()
                        .id(1L)
                        .name("Captain daughter")
                        .authorName("A.S.Pushkin")
                        .genreName("Roman")
                        .publicationDate(LocalDate.of(2020, 2, 2))
                        .build()));
    }

}
