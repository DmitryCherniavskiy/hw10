package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.general.AbstractEntity;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
public class Book implements AbstractEntity<Book> {
    private Long id;
    private String name;
    private String authorName;
    private LocalDate publicationDate;
    private String genreName;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long entityId) {
        id = entityId;
    }

    @Override
    public Book getCopy() {
        return new Book(id, name, authorName, publicationDate, genreName);
    }
}
