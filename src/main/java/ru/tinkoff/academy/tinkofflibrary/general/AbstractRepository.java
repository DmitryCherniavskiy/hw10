package ru.tinkoff.academy.tinkofflibrary.general;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AbstractRepository<T extends AbstractEntity<T>>{
    protected List<T> entities;

    public AbstractRepository(List<T> entities){
        this.entities = entities;
    }
    public List<T> getAll(){
        return new ArrayList<T>(entities);
    }

    public Optional<T> findById(Long id){
        List<T> list = entities.stream()
                .filter(x -> x.getId().equals(id))
                .map(AbstractEntity::getCopy)
                .toList();
        if (list.size() != 1){
            return Optional.empty();
        }else{
            return Optional.of(list.get(0));
        }

    }

    public T save(T entity){
        entity.setId(entities.get(entities.size()-1).getId() + 1);
        entities.add(entity.getCopy());
        return entity;
    }

    public boolean removeById(Long entityId){
        return entities.removeIf(x -> x.getId().equals(entityId));
    }

}
