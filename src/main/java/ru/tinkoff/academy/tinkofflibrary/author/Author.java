package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.general.AbstractEntity;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Author implements AbstractEntity<Author> {
    private Long id;
    private String AuthorName;

    @Override
    public Author getCopy() {
        return null;
    }
}
