package ru.tinkoff.academy.tinkofflibrary.author;

import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.general.AbstractRepository;

import java.util.List;

@Repository
public class AuthorRepository extends AbstractRepository<Author> {
    public AuthorRepository() {
        super(List.of(
                Author.builder()
                        .id(1L)
                        .AuthorName("A.S.Pushkin")
                        .build(),
                Author.builder()
                        .id(2L)
                        .AuthorName("I.S.Turgenev")
                        .build()
        ));
    }
}
