package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.general.EntityNotFoundException;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorService {
    final private AuthorRepository authorRepository;
    public Optional<Author> findById(Long authorId){
        return authorRepository.findById(authorId);
    }

    public Author getById(Long authorId){
        return authorRepository.findById(authorId).orElseThrow(() -> new EntityNotFoundException("Genre didn't found. Genre id = " + authorId));
    }
}
